
var filters = {
    fold: function(data, folds) {
        if(!folds) folds = 10;

        var trainingSet = [];
        var testingSet = [];
        var counter = 0;
        for(var item in data) {
            if(counter == 0 || !(counter % folds)) {
                testingSet.push(item);
            }
            else {
                trainingSet.push(data[item]);
            }
            counter++;
        }
        return {
            trainingSet,
            testingSet
        };
    }
}

module.exports = filters;