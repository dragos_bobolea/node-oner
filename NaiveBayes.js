var NaiveBayes = {
    buildClassifier: function(arff) {
        this.rules = buildClassifierRules(arff);
        this.baseProbabilities = getBaseProbabilitiesByClass(arff);
        this.classAttribute = arff.attributes[arff.attributes.length - 1];
        this.attributes = arff.attributes;
        this.attributes.splice(this.attributes.length-1,1);
        this.classes = arff.types[this.classAttribute].oneof;
        
    },
    getProbabilityForItem(item) {
        var probabilities = {};
        
        var sumForNormalizing = 0;
        var maxProbability = 0;
        var maxClass = null;
        
        for(var classKey in this.classes) {
            var probability = this.baseProbabilities[classKey];
            for(var attrName of this.attributes) {
                var rule = this.rules[attrName];
                var newProb = rule.probabilityFunction(classKey,item[attrName]); 
                probability *= newProb;
                    
            }

            if(probability > maxProbability) {
                maxProbability = probability;
                maxClass = classKey;
            }
            sumForNormalizing += probability;
        }
        
        probability = probability / sumForNormalizing * 100;
        console.log('Prediction: ' + this.classes[maxClass] + ' ' + probability + '%')
    
    }
}

function getBaseProbabilitiesByClass(arff) {
    
    var className = arff.attributes[arff.attributes.length - 1];
    var classes = arff.types[className].oneof;
    var baseProbabilities = {}

    for(var item of arff.data) {
        if(!baseProbabilities[item[className]]) {
            baseProbabilities[item[className]] = 0;
        }
        baseProbabilities[item[className]]++;
    }

    for(var classKey in classes) {
        baseProbabilities[classKey] /= arff.data.length;
    }
    
    return baseProbabilities;
}

function buildClassifierRules(arff) {
    // We loop the whole data set
    // For each attribute, we do the following
    // For nominal attributes, we count the number of occurences for each class
    // For numeric attributes, we calculate the gaussian distribution for each class
    // Since we need two passes for numeric attributes (we need the mean for the standard deviation)
    // We might as well loop once for each attribute: code is cleaner, performance is pretty much the same
    var rules = {};
    var className = arff.attributes[arff.attributes.length - 1];
    var classes = arff.types[className].oneof;
    var attributeNames = arff.attributes;
    for(var attrNameKey in attributeNames) {
        var attributeName = attributeNames[attrNameKey];
        
        if(arff.types[attributeName].type == 'nominal') {
            var ruleForAttribute = getRuleForNominalAttribute(attributeName, arff.types[attributeName].oneof,
                className, classes, arff.data);
            rules[attributeName] = ruleForAttribute;
        } else if(arff.types[attributeName].type == 'numeric') {
            var ruleForAttribute = getRuleForNumericAttribute(attributeName, className, classes, arff.data);
            rules[attributeName] = ruleForAttribute;
        }
    }
    
    return rules;
}


function getRuleForNumericAttribute(attributeName, className, classes, data) {
    var rule = {
        attributeName,
        type: 'numeric',
        distributionPerClass: {}
    }

    for(var classKey in classes) {
        rule.distributionPerClass[classKey] = {
            mean: 0,
            count: 0,
            standardDeviation: 0
        }
    }
    // Calculate the mean for each class
    for(var item of data) {
        rule.distributionPerClass[item[className]].count++;
        rule.distributionPerClass[item[className]].mean += item[attributeName];
    }

    for(var classKey in classes) {
        rule.distributionPerClass[classKey].mean = 
            rule.distributionPerClass[classKey].mean / rule.distributionPerClass[classKey].count;
    }

    // Calculate the standard deviation for each class
    for(var item of data) {
        rule.distributionPerClass[item[className]].standardDeviation += 
            Math.pow(item[attributeName] - rule.distributionPerClass[item[className]].mean, 2);
    }
    
    for(var classKey in classes) {
        rule.distributionPerClass[classKey].standardDeviation = Math.sqrt(
            rule.distributionPerClass[classKey].standardDeviation / (rule.distributionPerClass[classKey].count - 1)
        );
    }

    rule.probabilityFunction = getRuleFunctionForNumericAttribute(rule);
    return rule;
}
function getRuleForNominalAttribute(attributeName, attributeValues, className, classes, data) {
    var rule = {
        attributeName,
        type: 'nominal',
        countsPerClass: {},
    }
    for(var classKey in classes) {
        rule.countsPerClass[classKey] = {
            classCount: 0,
            countPerAttributeValue: {}
        };
        for(var attributeKey in attributeValues) {
            rule.countsPerClass[classKey].countPerAttributeValue[attributeKey] = 0;
        }
    }
    for(var item of data) {
        rule.countsPerClass[item[className]].classCount++;
        rule.countsPerClass[item[className]].countPerAttributeValue[item[attributeName]]++;
    }

    rule.probabilityFunction = getRuleFunctionForNominalAttribute(rule);
    return rule;
}

function getRuleFunctionForNumericAttribute(rule) {
    // Probability funciton for a class, given the attribute value
    // Using Gaussian distribution probability density function 
    // f(x) = e ^ - ((x - mean)^2 / (2 * stdDev ^ 2)) / (sqrt(2 * pi) * stdDev)
    return (classKey, attributeValue) => {
        var mean = rule.distributionPerClass[classKey].mean;
        var stdDev = rule.distributionPerClass[classKey].standardDeviation;
        var x = attributeValue;
        var e = Math.E;
        var PI = Math.PI;
        var sqrt = Math.sqrt;
        var pow = Math.pow;
        return pow(e, -(pow(x - mean,2) / (2 * pow(stdDev,2)))) / (sqrt(2 * PI) * stdDev);
    }
}

function getRuleFunctionForNominalAttribute(rule) {
    // Probability function for a class, given the attribute value
    // Using Laplace estimator
    return (classKey, attributeValue) => {
        return (rule.countsPerClass[classKey].countPerAttributeValue[attributeValue] + 1) / rule.countsPerClass[classKey].classCount;
    }
}
module.exports = NaiveBayes;