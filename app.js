var arff = require('node-arff')
var OneR = require('./OneR.js')
var NaiveBayes = require('./NaiveBayes.js')
var filters = require('./DataFilters.js')

if(process.argv.length < 4) {
    console.log('First argument must be the algorithm. Second argument must be the ARFF filepath.')
}
var algorithm = eval(process.argv[2]);
if(!algorithm) {
    console.log('Invalid algorithm.');
    return;
}
var arffFile = process.argv[3];

arff.load(arffFile, function (err, arff) {
    if (err) {
        return console.error(err);
    }

    run(arff);
});


function run(arff) {
    var originalData = arff.data;
    var accuracy = 0;
    // for(var i = 0; i < 1000; i++) {
    //     arff.data = originalData;
    //     arff.randomize();
    //     var data = filters.fold(arff.data);
    //     arff.data = data.trainingSet;
    //     OneR.buildClassifier(arff);
        
    //     accuracy += OneR.classify(data.trainingSet);
    // }
    // accuracy /= 1000;
    
    algorithm.buildClassifier(arff);

    algorithm.getProbabilityForItem({
        outlook:0,
        temperature:66,
        humidity:90,
        windy:0
    })
}



