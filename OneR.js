var OneR = {
    buildClassifier: function(arff, minBucketSize) {
        this.classAttribute = arff.attributes[arff.attributes.length - 1];
        if(!minBucketSize) {
            minBucketSize = 3
        }
        this.rule = buildClassifier(arff, minBucketSize, this.classAttribute);
    },
    classify: function(data) {
        var errorCount = 0;
        for(var i in data) {
            var item = data[i];
            var prediction = this.rule.classifyFunction(item); 
            if(item[this.classAttribute] != prediction) {
                errorCount++;
            }
                       
            
        }
        console.log('Correct: ' + (data.length - errorCount))
        console.log('Incorrect: ' + errorCount)
        return 1 - errorCount / data.length;
    }

}




function buildClassifier(arff, minBucketSize, classAttr) {
    // Associative array, one entry for each attribute
    // Each attribute has the rules for each value, and the total error count (false positives)
    var rules = [];
    
    var classValues = arff.types[classAttr].oneof;

    arff.attributes.every(attribute => {
        if (attribute == classAttr) return false;
        rules[attribute] = {
            errorCount: 0,
            rules: {}
        };

        var ruleForAttribute = null;
        switch(arff.types[attribute].type) {
            case 'nominal':
            ruleForAttribute = getNominalAttributeRule(attribute, classAttr,
                arff.types[attribute].oneof
                , arff.data);
            break;
            case 'numeric':
            ruleForAttribute = getNumericAttributeRule(attribute, classAttr, 
                minBucketSize, arff.data);
            break;
        }

        rules[attribute] = ruleForAttribute;
    
        return true;
    });

    var finalRule = getBestAttributeRules(rules);

    
    return finalRule;
}
function getBestAttributeRules(rules) {
    var finalRule = null;
    var minErrorCount = -1;
    for(var attr in rules) {
        var rule = rules[attr];
        if(minErrorCount < 0 || minErrorCount > rule.errorCount) {
            finalRule = {
                name: attr,
                errorCount: rule.errorCount,  
                classifyFunction: rule.classifyFunction,              
                rules: rule.rules
            }
            minErrorCount = finalRule.errorCount;
        }
    }

    return finalRule;
}
function getNumericAttributeRule(attributeName, classAttributeName, minBucketSize, data) {
    var rulesForAttribute = {
        attributeName,
        errorCount: 0,
        rules: {}
    };
    var buckets = [];
    // Prepare the data for partitioning
    data.sort((a,b) => a[attributeName] - b[attributeName]);

    // Partitioning rule:
    // Starting at the beginning of the training set
    // Mark the beginning of a bucket
    // Loop until there are at least minBucketSize of one class
    // Make that class the bucket class
    // Continue looping until the instance has a different class
    // Mark the ending of the bucket
    // Repeat
    // When done, merge adjacent buckets of the same majority class
    var classCounters = {};
    var bucketCount = 0;
    var bucket = null;
    
    for(var item of data) {
        if(bucket && (typeof bucket.class !== 'undefined')  && (item[classAttributeName] != bucket.class)) {
            bucket.errorCount = bucketCount - classCounters[bucket.class];
            buckets.push(bucket);
            rulesForAttribute.errorCount += bucket.errorCount;
            bucket = null;   
        }
        // If at the beginning or bucket was just finished, init a new one
        if(!bucket) {
            bucket = {
                minValue: item[attributeName]
            }
            classCounters = {};
            bucketCount = 0;
        }

        // Increment counter for this class
        if(!classCounters[item[classAttributeName]]) {
            classCounters[item[classAttributeName]] = 0;
        } 

        classCounters[item[classAttributeName]]++;
        if( classCounters[item[classAttributeName]] >= minBucketSize) {
            bucket.class = item[classAttributeName];
        }
        bucket.maxValue = item[attributeName];
        bucketCount++;
    }
    // There is a change that the last bucket didn't pass the minBucketSize for any class
    // We find the max counter and assign that class to the bucket
    maxClassKey = null;
    maxClassCount = 0;
    for(var classKey in classCounters) {
        if(maxClassCount < classCounters[classKey]) {
            maxClassKey = classKey;
            maxClassCount = classCounters[classKey];
        }
    }
    bucket.class = maxClassKey;
    bucket.errorCount = bucketCount - classCounters[bucket.class];
    buckets.push(bucket);
    rulesForAttribute.errorCount += bucket.errorCount;

    for(var i = 0; i < buckets.length - 1; i++) {
        if(buckets[i].class == buckets[i+1].class) {
            buckets[i].maxValue = buckets[i+1].maxValue;
            buckets[i].errorCount += buckets[i+1].errorCount;
            buckets.splice(i+1,1);
            i--;
        }
    }
    
    rulesForAttribute.rules = buckets;

    // Set the rule boundaries as the average value between the adjacent values
    // Ex. if rule X has maxValue 10 and rule X+1 has minValue 20, we change them both to 15
    for(var i = 0; i < rulesForAttribute.rules.length - 1; i++) {
        var avg = (rulesForAttribute.rules[i].maxValue + rulesForAttribute.rules[i+1].minValue) / 2;
        rulesForAttribute.rules[i].maxValue = avg;
        rulesForAttribute.rules[i+1].minValue = avg;
    }
    rulesForAttribute.classifyFunction = getNumericAttributeRuleFunction(rulesForAttribute);
    return rulesForAttribute;
}
function getNominalAttributeRuleFunction(rule) {
    var func = function(item) {
        return rule.rules[item[rule.attributeName]].outcome;
    };
    return func;
}
function getNumericAttributeRuleFunction(rule) {
    var func = function(item) {
        for(var i = 0; i < rule.rules.length; i++) {
            
            if((i == 0 || item[rule.attributeName] > rule.rules[i].minValue)
                && (i == rule.rules.length -1 || item[rule.attributeName] <= rule.rules[i].maxValue)) {
                    return rule.rules[i].class;
            }
        }
    };

    return func;
}
function getNominalAttributeRule(attributeName, classAttributeName, attributeValues, data) {
    var rulesForAttribute = {
        attributeName,
        errorCount: 0,
        rules: {}
    };
    attributeValues.forEach((attrValue, attrIndex) => {
        // Find the most frequent class
        var classCounters = {};
        var counterItemsWithThisAttributeValue = 0;
        data.forEach(item => {
            if (item[attributeName] == attrIndex) {
                if (!classCounters[item[classAttributeName]])
                    classCounters[item[classAttributeName]] = 0;
                classCounters[item[classAttributeName]]++;
                counterItemsWithThisAttributeValue++;
            }
        });

        var mostCommonClass = null;
        var mostCommonClassCount = 0;
        for (var classCount in classCounters) {
            if (classCounters.hasOwnProperty(classCount)) {
                if (classCounters[classCount] > mostCommonClassCount) {
                    mostCommonClass = classCount;
                    mostCommonClassCount = classCounters[classCount];
                }
            }
        }
        
        rulesForAttribute.rules[attrIndex] = {
            outcome: mostCommonClass,
            errorCount: (counterItemsWithThisAttributeValue - mostCommonClassCount)
        };
        rulesForAttribute.errorCount += rulesForAttribute.rules[attrIndex].errorCount;
    });
    rulesForAttribute.classifyFunction = getNominalAttributeRuleFunction(rulesForAttribute);
    return rulesForAttribute;
}

module.exports = OneR;